# ArgoCD Demo

Install ArgoCD to the cluster

```sh
$ kubectl create namespace argocd
$ kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

Port-forward to ArgoCD Web UI

```sh
$ kubectl port-forward svc/argocd-server -n argocd 8080:443
# Get password
$ kubectl get secret argocd-initial-admin-secret -n argocd -o yaml | grep password: | awk '{print $2}' | base64 -d
```

Login with user `admin` and the password you get from previous step

## Resources

[ingress-nginx 4.7.1 · kubernetes/ingress-nginx (artifacthub.io)](https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx)
